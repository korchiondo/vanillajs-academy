(function() {

	var usersStorage = [],
		errorPage = document.getElementById("error-page"),
		mainPage = document.getElementById("main-page");
	
	function init() {		
		request('GET', 'https://api.github.com/users?since=135')
    		.then(function (e) {
    			setUsersStorage(e.target.responseText);
    			renderUsers();
    			window.dispatchEvent(new HashChangeEvent("hashchange"));
   			}
    	);
    			
		window.addEventListener("hashchange", function() {
			render(decodeURI(window.location.hash));
		}, false);
	};

	init();

	function render(url) {
		hideErrorPage();
		var temp = url.split('/')[0];
		var map = {
			'': function() {
				clearUserDetails();
			},
			'#user': function() {
				var index = url.split('#user/')[1].trim();
				getUserDetails(index)
					.then(function(e) {
						renderUserDetails(e.currentTarget.responseText, index);
					}).catch(function() {
						showErrorPage();
					});
				
			},
		};
		if(map[temp]){
			map[temp]();
		}
		else {
			showErrorPage();
		}	
	}
	
	function getUserDetails(index) {
		var user = getUserByIndex(index);
		if(!user || !user.url) {
			user = {
				url: ""
			};
		}
		return request('GET', user.url);	
	}
	
	function renderUsers() {
		var users = getUsersStorage();
		for(var key in users) {
			var template = document.getElementById("grid-template").innerHTML,
			el = document.createElement('div');
			el.setAttribute("data-index", users[key].index);
			el.innerHTML = template;
			el.getElementsByClassName("avatar")[0].getElementsByTagName("img")[0].src = users[key].avatar_url;
		    el.getElementsByClassName("login")[0].innerHTML += users[key].login;
		    el.getElementsByClassName("admin")[0].innerHTML += users[key].site_admin;
		    
		    document.getElementById("users-list").appendChild(el);
		    
		    el.getElementsByClassName("row")[0].addEventListener('click', function (e) {
		    	e.preventDefault();
				var index = e.currentTarget.parentNode.getAttribute('data-index');
      			window.location.hash = 'user/' + index;
		    });
		}
	}
	
	function renderUserDetails(data, index) {
		clearUserDetails();
		var user = JSON.parse(data),
			template = document.getElementById("details-template").innerHTML,
			container = document.querySelectorAll("[data-index='" + index + "']")[0],
			el = container.getElementsByClassName("user-details")[0];
		
		el.innerHTML = template;
		el.getElementsByClassName("user-details-name")[0].innerHTML += user.name || "-";
		el.getElementsByClassName("user-details-email")[0].innerHTML += user.email || "-";
		el.getElementsByClassName("user-details-followers")[0].getElementsByTagName("a")[0].href = user.followers_url || "#";
		el.getElementsByClassName("user-details-followings")[0].getElementsByTagName("a")[0].href = user.following_url || "#";
		el.getElementsByClassName("user-details-starred")[0].getElementsByTagName("a")[0].href = user.starred_url || "#";
		el.getElementsByClassName("user-details-subscriptions")[0].getElementsByTagName("a")[0].href = user.subscriptions_url || "#";
		el.getElementsByClassName("user-details-organizations")[0].getElementsByTagName("a")[0].href =  user.organizations_url || "#";
		el.getElementsByClassName("user-details-repos")[0].getElementsByTagName("a")[0].href = user.repos_url || "#";
		
		el.classList.add("active");
	}

	function showErrorPage() {
    	errorPage.className = "";
    	mainPage.className = "hidden";
	}
	
	function hideErrorPage() {
		errorPage.className = "hidden";
    	mainPage.className = "container";
	}
	
	function clearUserDetails() {
		var elements = document.getElementsByClassName("user-details");
		for(var i in elements) {
			elements[i].className="user-details";
			elements[i].innerHTML = "";
		}
		hideErrorPage();
		return;
	}
	
	function setUsersStorage(data) {
		usersStorage = [];
		var users = JSON.parse(data);
		for(var key in users) {
			usersStorage.push({
				index: key,
				login: users[key].login,
				url: users[key].url,
				site_admin:users[key].site_admin,
				avatar_url:users[key].avatar_url
			});
		};
	}
	
	function getUsersStorage() {
		return usersStorage;
	}
	
	function getUserByIndex(index) {
		var users = getUsersStorage();
		for(var key in users) {
			if(users[key].index === index) {
				return users[key];
			}
		};
	}
	
	function request(method, url) {
	    return new Promise(function (resolve, reject) {
	        var xhr = new XMLHttpRequest();
	        xhr.open(method, url);
	        xhr.onload = resolve;
	        xhr.onerror = reject;
	        xhr.send();
	    });
	}
	
})();
